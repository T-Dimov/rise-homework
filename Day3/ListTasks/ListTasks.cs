﻿namespace ListTasks
{
    public class ListTasks
    {
        public static void Main()
        {
            int num_disks = 3;

            var source = new Stack<int>();
            var auxiliary = new Stack<int>();
            var target = new Stack<int>();

            for (int i = num_disks; i >= 1; i--)
            {
                source.Push(i);
            }

            Console.WriteLine(SolveHanoiTowers(num_disks, source, auxiliary, target, "Source", "Helper", "Target")); 
        }

        public static bool SolveHanoiTowers(int numDisks, Stack<int> source, Stack<int> helper, Stack<int> target, 
            string sourceName, string helperName, string targetName)
        {
            var finishedSuccessfully = false;

            if(numDisks == 0) 
            {
                return finishedSuccessfully;
            }

            if (numDisks == 1)
            {
                var disk = source.Pop();
                target.Push(disk);

                Console.WriteLine("Moved disk " + disk + " from " + sourceName + " to " + targetName);

                finishedSuccessfully = true;

                return finishedSuccessfully;
            }
            else
            {
                var success1 = SolveHanoiTowers(numDisks - 1, source, target, helper, sourceName, targetName, helperName);

                var disk = source.Pop();
                target.Push(disk);

                Console.WriteLine("Moved disk " + disk + " from " + sourceName + " to " + targetName);

                var success2 = SolveHanoiTowers(numDisks - 1, helper, source, target, targetName, sourceName, targetName);

                return success1 && success2;
            }
        }

        public static bool EraseMiddleElementInLinkedList(LinkedList<int> input)
        {
            if (input.Count == 0)
            {
                return false;
            }

            if (input.Count == 1)
            {
                input.RemoveFirst();

                return true;
            }

            var middleCount = (int)Math.Floor((decimal)input.Count / 2);
            var first = 0;

            for (int i = 0; i < middleCount; i++)
            {
                first = input.First();

                input.RemoveFirst();

                input.AddLast(first);
            }

            input.RemoveFirst();

            if (input.Count % 2 != 0)
            {
                middleCount--;
            }

            for (int i = 0; i < middleCount; i++)
            {
                first = input.First();

                input.RemoveFirst();

                input.AddLast(first);
            }

            return true;
        }

        public static List<int> GetUniqueElements(List<int> list)
        {
            var visited = new HashSet<int>();

            list.ForEach(x => visited.Add(x));

            return visited.ToList();
        }
    }
}