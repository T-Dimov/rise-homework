﻿namespace HashTasks
{
    using System.Linq;

    using System.Security.Cryptography;
    using System.Text;

    public class HashTasks
    {
        public class User
        {
            public string UserName { get; set; }

            public string Password { get; set; }

        }

        public static void Main()
        {
            CreateUser("Test", "123");
        }

        public static User CreateUser(string username, string password)
        {
            var user = new User();

            user.UserName = username;
            user.Password = HashPassword(password);

            return user;
        }

        public static string HashPassword(string secret)
        {
            using var sha256 = SHA256.Create();

            var secretBytes = Encoding.UTF8.GetBytes(secret);
            var secretHash = sha256.ComputeHash(secretBytes);

            return Convert.ToHexString(secretHash);
        }

        public static Dictionary<string, List<string>> GroupWordsByAnagrams(List<string> input, string[] sorted)
        {
            var result = new Dictionary<string, List<string>>();

            for (int i = 0; i < input.Count; i++)
            {
                var word = input[i];
                var sortedWord = string.Join("", sorted[i]);

                if (result.Keys.Any(x => sortedWord == string.Join("", x.OrderBy(y => y))))
                {
                    continue;
                }

                if (!result.ContainsKey(word))
                {
                    result[word] = new List<string> { word };
                }

                for (int j = i + 1; j < input.Count; j++)
                {
                    var searchWord = string.Join("", sorted[j]);

                    if (searchWord == sortedWord)
                    {
                        if (result[word].Contains(input[j]))
                        {
                            continue;
                        }

                        result[word].Add(input[j]);
                    }
                }
            }

            return result;
        }

        public static List<string> GetMisspelledWords(string input, HashSet<string> set)
        {
            var result = new List<string>();

            var inputWithoutPunctuation = input.Split(" ,-!.".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            foreach (var word in inputWithoutPunctuation)
            {
                if (set.Contains(word) == false)
                {
                    result.Add(word);
                }
            }

            return result;
        }

        public static List<string> GetAllNonRepeatingCharactersAndFirstNonRepeatingCharacterIndex(string input)
        {
            var result = new List<string>();
            var dict = new Dictionary<char, int>();

            for (int i = 0; i < input.Length; i++)
            {
                if (!dict.ContainsKey(input[i]))
                {
                    dict.Add(input[i], 1);
                }
                else
                {
                    dict[input[i]]++;
                }
            }

            var nonRepeating = dict.Where(x => x.Value == 1).Select(x => x.Key);

            result.Add(string.Join(", ", nonRepeating));

            if (nonRepeating.Any())
            {
                var firstChar = nonRepeating.First();

                result.Add("Index of first non-repeating character: " + input.IndexOf(firstChar));

                return result;
            }

            result.Add("Index of first non-repeating character: " + "-1");

            return result;
        }

        public static string[] GetIntersectingElements(string[] firstArray, string[] secondArray)
        {
            var combinedArrays = new string[firstArray.Length + secondArray.Length];
            firstArray.CopyTo(combinedArrays, 0);
            secondArray.CopyTo(combinedArrays, firstArray.Length);

            var set = new HashSet<string>();

            var result = new List<string>();

            foreach (var element in combinedArrays)
            {
                if (set.Add(element) == false)
                {
                    result.Add(element);
                }
            }

            return result.ToArray();
        }

        public static List<string> GetCarOwner(Dictionary<string, string> dict, string plateNumber)
        {
            var result = new List<string>();

            if (dict.ContainsKey(plateNumber) == false)
            {
                throw new Exception("This plate doesn't have an owner!");
            }

            var ownerName = dict[plateNumber];
            result.Add(ownerName);

            var allNames = dict.Select(x => x.Value).ToList();
            var namesWithMoreThanOneCar = allNames.GroupBy(x => x).Where(g => g.Count() > 1).Select(x => x.Key);

            if (namesWithMoreThanOneCar.Any())
            {
                result.Add("Owners with more than one car: ");
                result.Add(string.Join(", ", namesWithMoreThanOneCar));
            }

            return result;
        }
    }
}