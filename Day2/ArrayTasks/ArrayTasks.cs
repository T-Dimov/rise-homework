﻿namespace ArrayTasks
{
    public class ArrayTasks
    {
        public static void Main()
        {
            var input = 27;

            var result = FindCubeRoot(input);

            Console.WriteLine(result);
        }

        public static int FindCubeRoot(int num)
        {
            var counter = 1;

            if (num > 1000000)
            {
                counter = 100;
            }
            else if (num > 100000)
            {
                counter = 46;
            }
            else if (num > 10000)
            {
                counter = 21;
            }
            else if (num > 1000)
            {
                counter = 10;
            }

            while (true)
            {
                if (counter * counter * counter == num)
                {
                    return counter;
                }

                counter++;
            }
        }

        public static int FindMissingNumber(int[] arr)
        {
            var sorted = ArraySorted(arr);

            if (arr.Length == 2)
            {
                return arr[0] + 1;
            }

            for (int i = 1; i < arr.Length - 2; i++)
            {
                if (arr[i] != arr[i - 1] + 1)
                {
                    return arr[i - 1] + 1;
                }
            }

            return -1;
        }

        private static int[] ArraySorted(int[] arr)
        {
            int temp = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (arr[i] > arr[j])
                    {
                        temp = arr[j];
                        arr[j] = arr[i];
                        arr[i] = temp;
                    }
                }
            }

            return arr;
        }
    }
}